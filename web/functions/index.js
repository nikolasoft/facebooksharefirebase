const functions = require("firebase-functions");
const axios = require("axios");

const express = require("express");
const cors = require("cors")({origin: true});
const app = express();
app.use(cors);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

app.get("/postjm/:position", (req, res) => {
  const position = req.params.position;

  const postsImgsUrls = ["https://images.unsplash.com/photo-1519046904884-53103b34b206?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    "https://images.unsplash.com/photo-1525014341625-f264f9291dde?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=967&q=80",
    "https://images.unsplash.com/photo-1493246507139-91e8fad9978e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"];

  const postTitles = ["Post #1",
    "Post #2",
    "Post #3",
  ];

  const postSecondary = ["This is post #1",
    "This is post #2",
    "This is post #3",
  ];
  res.send(`
    <!doctype html>
    <head>
    <meta property="og:url"           content="https://blog-db24a.web.app/postjm/${position}" />
    <meta property="og:type"          content="website" />
     <meta property="og:title"         content="${postTitles[position]}" />
    <meta property="og:description"   content="${postSecondary[position]}" />
    <meta property="og:image"         content="${postsImgsUrls[position]}" />
    </head>
    <body>
      <p>Redireccionando...
    </body>
    <script>
    window.location = "/post.html?position="+${position}
    </script>
  </html>`);
  // res.redirect(__dirname + "/public/post.html?position="+position);
});


exports.myFunction = functions.firestore
    .document("collection/{docId}")
    .onWrite((change, context) => {
      console.log("Se llama a la funcion jmcollection");


      axios.post("https://oppwa.com/v1/checkouts", [], {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "Access-Control-Allow-Methods": "*",
          "Access-Control-Allow-Origin": "https://oppwa.com",
          "Access-Control-Allow-Headers": "Content-Type, Authorization",
          "Access-Control-Allow-Credentials": "true",
          "Access-Control-Max-Age": "3600",
          // eslint-disable-next-line max-len
          "Authorization": "Bearer OGFjOWE0Y2U3NzY3NmM4ZTAxNzc2OGU3OGFmYTIyMjB8TWRiZjNSOE02SA==",
        },
      })
          .then((response) => {
            console.log("<<<Se imprime la respuesta>>>");
            console.log(response.data);
          })
          .catch((err) => {
            console.log(err);
          });


      // axios.get("https://dog.ceo/api/breeds/image/random")
      //     .then(function(response) {
      //     // handle success
      //       console.log("Se consume el web service con exito");
      //       console.log(response);
      //     })
      //     .catch(function(error) {
      //     // handle error
      //       console.log("Error al consumir el web service");
      //       console.log(error);
      //     })
      //     .then(function() {
      //     // always executed
      //       console.log("Siempre se ejecuta web service");
      //     });
    });

exports.app = functions.https.onRequest(app);
