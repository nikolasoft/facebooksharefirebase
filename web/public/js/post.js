var postsImgsUrls = ["https://images.unsplash.com/photo-1519046904884-53103b34b206?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    "https://images.unsplash.com/photo-1525014341625-f264f9291dde?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=967&q=80",
    "https://images.unsplash.com/photo-1493246507139-91e8fad9978e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"];

let postTitles = ["Post #1",
    "Post #2",
    "Post #3",
                    ]

let postSecondary = ["This is post #1",
    "This is post #2",
    "This is post #3",
]


let urlParams = new URLSearchParams(window.location.search)

let position = urlParams.get('position')

console.log("la posicion obtenida es: " + position)



$("#postImage").attr("src", postsImgsUrls[position]);

$("#titlePost").text(postTitles[position]);

$("#secondaryPost").text(postSecondary[position]);


 $("meta[property='og:url']").attr("content", "https://blog-db24a.web.app/post.html?position=" + position);
 $("meta[property='og:title']").attr("content", postTitles[position]);
 $("meta[property='og:description']").attr("content", postSecondary[position]);
 $("meta[property='og:image']").attr("content", postsImgsUrls[position]);


$(function () {
    console.log("ready Post!");


    // let src= "https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fblog-db24a.web.app%2Fpost.html%3Fposition%3D"+position+"&layout=button&size=small&appId=472438607090092&width=89&height=20"
    // $("#facebookShareIframe").attr('src', "https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fblog-db24a.web.app%2Fpost.html%3Fposition%3D" + position + "&layout=button&size=small&appId=472438607090092&width=89&height=20");

    // console.log("El link generado es el siguiente")
    // console.log(src)
    // $('#facebookShare').data('href', "https://blog-db24a.web.app/post.html?position=" + position)
    // $("#facebookShare").prop("data-href", "https://blog-db24a.web.app/post.html?position=" + position);

    // $("meta[property='og:url']").attr("content", "https://blog-db24a.web.app/post.html?position=" + position);
    // $("meta[property='og:title']").attr("content", postTitles[position]);
    // $("meta[property='og:description']").attr("content", postSecondary[position]);
    // $("meta[property='og:image']").attr("content", postsImgsUrls[position]);


    var link = document.createElement('meta');
    link.setAttribute('property', 'og:url');
    link.content = "https://blog-db24a.web.app/post.html?position=" + position;
    document.getElementsByTagName('head')[0].appendChild(link);

    var link2 = document.createElement('meta');
    link2.setAttribute('property', 'og:title');
    link2.content = postTitles[position];
    document.getElementsByTagName('head')[0].appendChild(link2);

    var link3 = document.createElement('meta');
    link3.setAttribute('property', 'og:description');
    link3.content = postSecondary[position];
    document.getElementsByTagName('head')[0].appendChild(link3);

    var link4 = document.createElement('meta');
    link4.setAttribute('property', 'og:image');
    link4.content = postsImgsUrls[position];
    document.getElementsByTagName('head')[0].appendChild(link4);


    window.fbAsyncInit = function () {
        FB.init({
            appId: '472438607090092',
            autoLogAppEvents: true,
            xfbml: true,
            version: 'v2.10'
        });
        FB.AppEvents.logPageView();
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
});

$(document).on('click', '#getMetasButton', function () {




    // $("meta[property='og:url']").attr("content", "https://blog-db24a.web.app/post.html?position=" + position);
    // $("meta[property='og:title']").attr("content", postTitles[position]);
    // $("meta[property='og:description']").attr("content", postSecondary[position]);
    // $("meta[property='og:image']").attr("content", postsImgsUrls[position]);

    // console.log($("meta[property='og:url']").attr("content"))
    // console.log($("meta[property='og:title']").attr("content"))
    // console.log($("meta[property='og:description']").attr("content"))
    // console.log($("meta[property='og:image']").attr("content"))

    FB.ui({
        method: 'share',
        href: "https://blog-db24a.web.app/postjm/" + position,
    }, function (response) { 

        console.log("Se imprime la respuesta "+JSON.stringify(responsef))
    });

    // FB.ui({
    //     method: 'share_open_graph',
    //     action_type: 'og.shares',
    //     action_properties: JSON.stringify({
    //         object: {
    //             'og:url': "https://blog-db24a.web.app/postjm/" + position,
    //             'og:title': postTitles[position],
    //             'og:description': postSecondary[position],
    //             'og:image': postsImgsUrls[position],

    //         }
    //     })
    // },
    //     function (response) {
    //         console.log("Se obtiene la siguiente respuesta: "+response)

    //     });
    
});



// $('#target').attr('src', 'https://example.com/img.jpg?rand=' + Math.random());

